#!/usr/bin/env python

from data_rdb import Rethink_DBS
import asyncio
import uvloop
from pathlib import Path
import ujson as json
import click
from tasktools.taskloop import coromask, renew, simple_fargs, simple_fargs_out
from networktools.time import get_datetime_di
import functools

def read_conf(conf):
    filepath = Path(conf)
    keys = {"host","port", "dbname", "table_name", "log_path", "key", "station"}
    if filepath.exists():
        with open(filepath,"r") as f:
            data = json.load(f)
            print("Data on file", data.keys(), "keys", keys)
            print(list(map(lambda e: e in data.keys(), keys)))
            if all(map(lambda e: e in data.keys(), keys)):
                print("No están todos los parámetros suficientes")
            return data, keys
    else:
        print("Por favor, entrega una ruta correcta para el archivo de parámetros")
        print("Además debe contener las siguientes llaves")
        archivo = Path(__file__).parent/"ejemplo_rdb.json"
        if archivo.exists():
            with open(archivo) as f:
                data = json.load(f)
                print("{")
                [print("    %s:%s,"%(k,v)) for k,v in data.items()]
                print("}")
        return {}, keys



def db_instance(conf, **kwargs):
    try:
        opts, keys = read_conf(conf)
        if opts:
            opts.update(kwargs)
            return Rethink_DBS(**opts), opts
        else:
            print("No están todos los parámetros necesarios para crear una conexión")
            print(keys)
            return None, opts
    except Exception as ex:
        print("Error creando instancia database con datos de %s" % conf)
        raise ex


async def get_data(rdb, d0, table_name, key='DT_GET'):
    try:
         df = rdb.iso8601(get_datetime_di(delta=0))
         # now
         filter_opt = {'left_bound': 'open', 'index': key}
         cursor = await rdb.get_data_filter(
             table_name,
             [d0, df],
             filter_opt,
             key)
         return cursor, df
    except Exception as ex:
         print("Exception al intentar obtener datos", ex)
         print("Error en obtención de data desde rethinkdb")
         rdb.logger.error("Error en la obtención de datos para estación %s en %s" % (table_name, d0))
         return [], df

            
async def cycle(rdb, *args, **kwargs):
    if kwargs["connect"]:
        result = await rdb.async_connect()
        kwargs["connect"] = False
        print("resultado de conectarse", result)
    print("On cycle:", rdb, args, kwargs)
    file_obj = kwargs.get("file")
    d0 = kwargs.get('d0')
    table_name = kwargs.get("table_name")
    key = kwargs.get('key')
    dormir = kwargs.get("dormir")
    cursor, df = await get_data(rdb, d0, table_name, key)
    kwargs["counter"] += len(cursor)
    print("Cantidad de elementos en cursor: ",len(cursor))
    print("df", df)
    for elem in cursor:
        [print("%s -> %s" %(k,v)) for k,v in elem.items()]
        if file_obj:
            json.dump(elem, indent=2)
            file_obj.write(",")
    kwargs["d0"] = df
    await asyncio.sleep(dormir)
    if kwargs["counter"]>kwargs["limit"]:
        loop = asyncio.get_event_loop()
        if file_obj:
            file_obj.write("]")
            file_obj.close()
        loop.close()
    return [rdb, *args], kwargs
  

def rdb_create_task(loop, args, kwargs):
    print("Creando tarea loop")
    print(args, kwargs)
    task = loop.create_task(coromask(cycle,args,kwargs,simple_fargs_out))
    task.add_done_callback(functools.partial(renew,task,cycle,simple_fargs_out))    

import re
import os


def run_cycle(conf,  station, table_name, limite=2000,  destino='tabla.json', sleep=2):
    json_file = re.compile("\.json$")
    new_loop =uvloop.new_event_loop()
    asyncio.set_event_loop(new_loop)
    loop = asyncio.get_event_loop()
    kwargs = {}
    kwargs["counter"] = 0
    kwargs["dormir"] = sleep
    kwargs["limit"] = limite
    kwargs["code"] = station
    kwargs["table_name"] = table_name
    kwargs["connect"] = True
    rdb, opts = db_instance(conf,**kwargs)
    kwargs["d0"] = rdb.iso8601(get_datetime_di(delta=0))    
    if rdb:
        kwargs.update(opts)
        args = [rdb]
        filepath = Path(destino)    
        if json_file.search(destino):
            print("Destino válido:", destino)
            if not filepath.parent.exists():
                os.makedirs(filepath.parent)        
            with open(filepath,'w') as filedata:
                filedata.write("[")
                kwargs["file"]=filedata
                task = rdb_create_task(loop, args, kwargs)
                if not loop.is_running():
                    loop.run_forever()
        else:
            kwargs["file"]=False
            task = rdb_create_task(loop, args, kwargs)
            if not loop.is_running():
                loop.run_forever()
    else:
        print("No se pudo activar la sesión para Rethinkdb, revisa si tus parámetros son correctos")
        

@click.command()
@click.option("--conf", default="rdb_data.json", help="Archivo json de configuración",show_default=True)
@click.option("--limite", default=200, help="Cantidad de muestras máxima",show_default=True,type=int)
@click.option("--destino", default="data_rdb.json", help="Archivo destino de los datos",show_default=True)
@click.option("--sleep", default=2, help="Cantidad de segundos para descanzar la iteración",show_default=True, type=int)
@click.option("--station", default="VALN", help="Nombre de estación",show_default=True)
@click.option("--table_name", default="VALN_GSOF", help="Nombre de tabla",show_default=True)
def run_rdb(conf,  station, table_name, limite, destino, sleep):
    run_cycle(conf, station, table_name, limite, destino, sleep)
    


if __name__ == "__main__":
    run_rdb()

