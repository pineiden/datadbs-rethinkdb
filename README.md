Data RDB
========


Este módulo provee conexión a bases de datos RethinkDB con características asíncronas.


Se debe entregar (host, port, code) para conectarse.

Permite:

- crear database
- crear tablas
- añadir datos

También contiente un script de pruebas 'crea_perfiles.py' para probar
la operación en una RDB.